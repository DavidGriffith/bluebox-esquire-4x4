EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Bluebox, Esquire model"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "This one is a reimagining of that project using an AVR ATtiny85 instead."
Comment4 "The bluebox presented at projectmf.org is based on the PIC12F683."
$EndDescr
$Comp
L bluebox-esquire-4x4-rescue:ATTINY85-P-bluebox-esquire-4x4-rescue IC1
U 1 1 57A3232E
P 7525 4500
F 0 "IC1" H 7525 5017 50  0000 C CNN
F 1 "ATTINY85-P" H 7525 4926 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7525 4925 50  0001 C CIN
F 3 "http://www.mouser.com/ds/2/36/Atmel-2586-AVR-8-bit-Microcontroller-ATtiny25-ATti-276549.pdf" H 7525 4500 50  0001 C CNN
F 4 "http://www.atmel.com/devices/attiny85.aspx" H 7525 4500 60  0001 C CNN "Web page"
	1    7525 4500
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:GND-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR01
U 1 1 57A32980
P 3575 4600
F 0 "#PWR01" H 3575 4350 50  0001 C CNN
F 1 "GND" H 3580 4427 50  0000 C CNN
F 2 "" H 3575 4600 50  0000 C CNN
F 3 "" H 3575 4600 50  0000 C CNN
	1    3575 4600
	-1   0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:GND-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR02
U 1 1 57A336FA
P 8275 3925
F 0 "#PWR02" H 8275 3675 50  0001 C CNN
F 1 "GND" H 8280 3752 50  0000 C CNN
F 2 "" H 8275 3925 50  0000 C CNN
F 3 "" H 8275 3925 50  0000 C CNN
	1    8275 3925
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:GND-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR03
U 1 1 57A348AC
P 4625 3975
F 0 "#PWR03" H 4625 3725 50  0001 C CNN
F 1 "GND" H 4630 3802 50  0000 C CNN
F 2 "" H 4625 3975 50  0000 C CNN
F 3 "" H 4625 3975 50  0000 C CNN
	1    4625 3975
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:VCC-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR04
U 1 1 57A3E4E4
P 8925 4125
F 0 "#PWR04" H 8925 3975 50  0001 C CNN
F 1 "VCC" H 8942 4298 50  0000 C CNN
F 2 "" H 8925 4125 50  0000 C CNN
F 3 "" H 8925 4125 50  0000 C CNN
	1    8925 4125
	-1   0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:GND-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR05
U 1 1 57A3E622
P 8925 4800
F 0 "#PWR05" H 8925 4550 50  0001 C CNN
F 1 "GND" H 8930 4627 50  0000 C CNN
F 2 "" H 8925 4800 50  0000 C CNN
F 3 "" H 8925 4800 50  0000 C CNN
	1    8925 4800
	-1   0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:VCC-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR06
U 1 1 57A40674
P 7050 2000
F 0 "#PWR06" H 7050 1850 50  0001 C CNN
F 1 "VCC" H 7067 2173 50  0000 C CNN
F 2 "" H 7050 2000 50  0000 C CNN
F 3 "" H 7050 2000 50  0000 C CNN
	1    7050 2000
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:GND-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR07
U 1 1 57A406A6
P 7050 2975
F 0 "#PWR07" H 7050 2725 50  0001 C CNN
F 1 "GND" H 7055 2802 50  0000 C CNN
F 2 "" H 7050 2975 50  0000 C CNN
F 3 "" H 7050 2975 50  0000 C CNN
	1    7050 2975
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:VCC-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR08
U 1 1 57A441B7
P 2375 775
F 0 "#PWR08" H 2375 625 50  0001 C CNN
F 1 "VCC" H 2392 948 50  0000 C CNN
F 2 "" H 2375 775 50  0000 C CNN
F 3 "" H 2375 775 50  0000 C CNN
	1    2375 775 
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:GND-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR09
U 1 1 57A57634
P 2375 7625
F 0 "#PWR09" H 2375 7375 50  0001 C CNN
F 1 "GND" H 2380 7452 50  0000 C CNN
F 2 "" H 2375 7625 50  0000 C CNN
F 3 "" H 2375 7625 50  0000 C CNN
	1    2375 7625
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:GND-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR010
U 1 1 57A94DE7
P 6000 5575
F 0 "#PWR010" H 6000 5325 50  0001 C CNN
F 1 "GND" H 6005 5402 50  0000 C CNN
F 2 "" H 6000 5575 50  0000 C CNN
F 3 "" H 6000 5575 50  0000 C CNN
	1    6000 5575
	-1   0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:VCC-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR011
U 1 1 57A952CC
P 6000 5150
F 0 "#PWR011" H 6000 5000 50  0001 C CNN
F 1 "VCC" H 6017 5323 50  0000 C CNN
F 2 "" H 6000 5150 50  0000 C CNN
F 3 "" H 6000 5150 50  0000 C CNN
	1    6000 5150
	-1   0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:PWR_FLAG-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #FLG012
U 1 1 59C9F476
P 7300 2775
F 0 "#FLG012" H 7300 2850 50  0001 C CNN
F 1 "PWR_FLAG" H 7300 2925 50  0000 C CNN
F 2 "" H 7300 2775 50  0001 C CNN
F 3 "" H 7300 2775 50  0001 C CNN
	1    7300 2775
	-1   0    0    1   
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:LED-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue LED1
U 1 1 59CA2F69
P 5500 4125
F 0 "LED1" V 5500 3975 50  0000 C CNN
F 1 "LED" H 5500 4025 50  0001 C CNN
F 2 "LED_THT:LED_D1.8mm_W3.3mm_H2.4mm" H 5500 4125 50  0001 C CNN
F 3 "" H 5500 4125 50  0001 C CNN
	1    5500 4125
	0    1    1    0   
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:LED-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue LED2
U 1 1 5A72E754
P 5800 4125
F 0 "LED2" V 5800 3975 50  0000 C CNN
F 1 "LED" H 5800 4025 50  0001 C CNN
F 2 "LED_THT:LED_D1.8mm_W3.3mm_H2.4mm" H 5800 4125 50  0001 C CNN
F 3 "" H 5800 4125 50  0001 C CNN
	1    5800 4125
	0    1    1    0   
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:Battery_Cell-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue B2
U 1 1 5A7C4F38
P 4925 2975
F 0 "B2" V 4875 2875 50  0000 L CNN
F 1 "CR2032" V 4650 2850 50  0001 L CNN
F 2 "Battery:BatteryHolder_Keystone_103_1x20mm" V 4925 3035 50  0001 C CNN
F 3 "" V 4925 3035 50  0001 C CNN
F 4 "BH800S" V 4925 2975 60  0001 C CNN "Part Number"
	1    4925 2975
	0    -1   -1   0   
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:Battery_Cell-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue B1
U 1 1 5A7C4FFB
P 4925 2225
F 0 "B1" V 4875 2125 50  0000 L CNN
F 1 "CR2032" V 4650 2100 50  0001 L CNN
F 2 "Battery:BatteryHolder_Keystone_103_1x20mm" V 4925 2285 50  0001 C CNN
F 3 "" V 4925 2285 50  0001 C CNN
F 4 "BH800S" V 4925 2225 60  0001 C CNN "Part Number"
	1    4925 2225
	0    -1   -1   0   
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:PWR_FLAG-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #FLG013
U 1 1 5A7C6758
P 7300 2225
F 0 "#FLG013" H 7300 2300 50  0001 C CNN
F 1 "PWR_FLAG" H 7300 2375 50  0000 C CNN
F 2 "" H 7300 2225 50  0001 C CNN
F 3 "" H 7300 2225 50  0001 C CNN
	1    7300 2225
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:Conn_01x02-bluebox-esquire-4x4-rescue CONN1
U 1 1 5A7CD8DD
P 5825 2525
F 0 "CONN1" H 5875 2625 50  0000 C CNN
F 1 "EXTERNAL POWER" H 5975 2300 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 5825 2525 50  0001 C CNN
F 3 "" H 5825 2525 50  0001 C CNN
	1    5825 2525
	-1   0    0    -1  
$EndComp
Text Notes 4500 1700 0    60   ~ 0
For one cell in each holder: short 1-3 and 2-4.\nFor two cells in each holder: short 3-5 and 4-6. \nIf not using onboard cells, connect power to CONN1.
$Comp
L bluebox-esquire-4x4-rescue:Conn_01x02-bluebox-esquire-4x4-rescue CONN3
U 1 1 5A7DF364
P 8725 3625
F 0 "CONN3" H 8725 3725 50  0000 C CNN
F 1 "AUDIO OUT" H 9000 3600 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 8725 3625 50  0001 C CNN
F 3 "" H 8725 3625 50  0001 C CNN
	1    8725 3625
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:Conn_02x03_Odd_Even-bluebox-esquire-4x4-rescue CONN4
U 1 1 5A7E180E
P 5700 5375
F 0 "CONN4" H 5750 5575 50  0000 C CNN
F 1 "ISP" H 5750 5175 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 5700 5375 50  0001 C CNN
F 3 "" H 5700 5375 50  0001 C CNN
	1    5700 5375
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:Conn_02x03_Odd_Even-bluebox-esquire-4x4-rescue J1
U 1 1 5A7E2325
P 4825 2600
F 0 "J1" H 4875 2800 50  0000 C CNN
F 1 "SERIAL/PARALLEL" H 4850 2400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 4825 2600 50  0001 C CNN
F 3 "" H 4825 2600 50  0001 C CNN
	1    4825 2600
	1    0    0    -1  
$EndComp
$Comp
L bluebox-esquire-4x4-rescue:Conn_01x02-bluebox-esquire-4x4-rescue CONN2
U 1 1 5A7E5FD1
P 6275 2200
F 0 "CONN2" V 6375 2125 50  0000 C CNN
F 1 "POWER SWITCH" V 6450 2150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 6275 2200 50  0001 C CNN
F 3 "" H 6275 2200 50  0001 C CNN
	1    6275 2200
	0    -1   1    0   
$EndComp
Wire Wire Line
	5050 4650 6175 4650
Wire Wire Line
	5050 4750 5050 4650
Wire Wire Line
	5050 4550 6175 4550
Wire Wire Line
	5050 4450 5050 4550
Wire Wire Line
	6100 3625 6100 4250
Wire Wire Line
	6100 3625 7000 3625
Wire Wire Line
	8275 3925 8275 3725
Wire Wire Line
	8875 4750 8925 4750
Wire Wire Line
	8925 4750 8925 4800
Wire Wire Line
	2375 1075 2375 1125
Wire Wire Line
	2375 1475 2375 1525
Wire Wire Line
	2375 1525 2700 1525
Connection ~ 2375 1525
Wire Wire Line
	2375 1875 2375 1925
Wire Wire Line
	2375 1925 2700 1925
Connection ~ 2375 1925
Wire Wire Line
	2375 2275 2375 2325
Wire Wire Line
	2375 2325 2700 2325
Connection ~ 2375 2325
Wire Wire Line
	2375 2675 2375 2725
Wire Wire Line
	2375 2725 2700 2725
Connection ~ 2375 2725
Wire Wire Line
	2375 3075 2375 3125
Wire Wire Line
	2375 3125 2700 3125
Connection ~ 2375 3125
Wire Wire Line
	2375 3475 2375 3550
Wire Wire Line
	2375 3550 2725 3550
Connection ~ 2375 3550
Wire Wire Line
	2375 3925 2375 3975
Connection ~ 2375 3975
Wire Wire Line
	2375 4325 2375 4375
Wire Wire Line
	2375 4375 2725 4375
Connection ~ 2375 4375
Wire Wire Line
	2375 4725 2375 4775
Wire Wire Line
	2375 4775 2725 4775
Connection ~ 2375 4775
Wire Wire Line
	2375 5125 2375 5175
Connection ~ 2375 5175
Wire Wire Line
	2375 5525 2375 5575
Wire Wire Line
	2375 5575 2725 5575
Connection ~ 2375 5575
Wire Wire Line
	2375 5925 2375 5975
Wire Wire Line
	2375 5975 2725 5975
Connection ~ 2375 5975
Wire Wire Line
	2375 6325 2375 6375
Wire Wire Line
	2375 6375 2725 6375
Connection ~ 2375 6375
Connection ~ 3425 1925
Connection ~ 3425 2325
Connection ~ 3425 3125
Connection ~ 3425 4775
Connection ~ 3425 5975
Connection ~ 3425 5575
Connection ~ 3425 5175
Connection ~ 3425 4375
Connection ~ 3425 3975
Connection ~ 3425 3550
Connection ~ 3425 2725
Wire Wire Line
	3125 3975 3425 3975
Wire Wire Line
	6100 5375 6000 5375
Wire Wire Line
	6100 4250 6175 4250
Wire Wire Line
	5500 5275 5500 4350
Wire Wire Line
	6000 5275 6000 5150
Wire Wire Line
	6000 5475 6000 5575
Wire Wire Line
	5400 5375 5500 5375
Wire Wire Line
	5400 4350 5400 4450
Wire Wire Line
	5400 4450 6175 4450
Wire Wire Line
	6175 4750 5300 4750
Wire Wire Line
	5300 4750 5300 5475
Wire Wire Line
	5300 5475 5500 5475
Wire Wire Line
	3100 1525 3425 1525
Wire Wire Line
	3425 2725 3100 2725
Wire Wire Line
	3425 2325 3100 2325
Wire Wire Line
	3425 1925 3100 1925
Wire Wire Line
	3425 3125 3100 3125
Wire Wire Line
	3425 3550 3125 3550
Wire Wire Line
	3425 4375 3125 4375
Wire Wire Line
	3425 4775 3125 4775
Wire Wire Line
	3425 5175 3125 5175
Wire Wire Line
	3425 5575 3125 5575
Wire Wire Line
	3425 5975 3125 5975
Wire Wire Line
	3425 6375 3125 6375
Connection ~ 6100 4250
Wire Wire Line
	4200 4350 5400 4350
Connection ~ 5400 4450
Wire Wire Line
	8275 3725 8525 3725
Wire Wire Line
	4500 3975 4625 3975
Wire Wire Line
	7050 2975 6025 2975
Wire Wire Line
	5500 4275 5500 4350
Connection ~ 5500 4350
Wire Wire Line
	5800 4275 5800 4350
Connection ~ 5800 4350
Wire Wire Line
	5500 3875 5500 3975
Wire Wire Line
	5800 3975 5800 3875
Wire Wire Line
	5300 3575 5500 3575
Wire Wire Line
	7050 2000 7050 2225
Wire Wire Line
	7050 2000 6800 2000
Wire Wire Line
	6375 2000 6500 2000
Wire Wire Line
	6025 2625 6025 2975
Wire Wire Line
	6025 2525 6025 2000
$Comp
L bluebox-esquire-4x4-rescue:GND-bluebox-esquire-4x4-rescue-bluebox-esquire-4x4-rescue #PWR014
U 1 1 5AB0E688
P 5300 3675
F 0 "#PWR014" H 5300 3425 50  0001 C CNN
F 1 "GND" H 5305 3502 50  0000 C CNN
F 2 "" H 5300 3675 50  0000 C CNN
F 3 "" H 5300 3675 50  0000 C CNN
	1    5300 3675
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3575 5300 3675
Connection ~ 5500 3575
Wire Wire Line
	3425 1525 3425 1925
Wire Wire Line
	2375 3975 2725 3975
Wire Wire Line
	2375 1525 2375 1575
Wire Wire Line
	2375 1925 2375 1975
Wire Wire Line
	2375 2325 2375 2375
Wire Wire Line
	2375 2725 2375 2775
Wire Wire Line
	2375 3125 2375 3175
Wire Wire Line
	2375 3550 2375 3625
Wire Wire Line
	2375 3975 2375 4025
Wire Wire Line
	2375 4375 2375 4425
Wire Wire Line
	2375 4775 2375 4825
Wire Wire Line
	2375 5175 2375 5225
Wire Wire Line
	2375 5575 2375 5625
Wire Wire Line
	2375 5975 2375 6025
Wire Wire Line
	2375 6375 2375 6425
Wire Wire Line
	3425 1925 3425 2325
Wire Wire Line
	3425 2325 3425 2725
Wire Wire Line
	3425 3125 3425 3550
Wire Wire Line
	3425 4775 3425 5175
Wire Wire Line
	3425 5975 3425 6375
Wire Wire Line
	3425 5575 3425 5975
Wire Wire Line
	3425 5175 3425 5575
Wire Wire Line
	3425 4375 3425 4775
Wire Wire Line
	3425 3975 4200 3975
Wire Wire Line
	3425 3975 3425 4375
Wire Wire Line
	3425 3550 3425 3975
Wire Wire Line
	3425 2725 3425 3125
Wire Wire Line
	6100 4250 6100 5375
Wire Wire Line
	5400 4450 5400 5375
Wire Wire Line
	5500 4350 5800 4350
Wire Wire Line
	5800 4350 6175 4350
Wire Wire Line
	6025 2000 6275 2000
Wire Wire Line
	7050 2775 7050 2975
Wire Wire Line
	5500 3575 5800 3575
Wire Wire Line
	4725 2975 4525 2975
Wire Wire Line
	4525 2975 4525 2600
Wire Wire Line
	4525 2600 4625 2600
Wire Wire Line
	5025 2225 5325 2225
Wire Wire Line
	5325 2225 5325 2600
Wire Wire Line
	5325 2600 5125 2600
Wire Wire Line
	4425 2225 4725 2225
Connection ~ 6025 2975
Wire Wire Line
	6025 2000 4425 2000
Wire Wire Line
	4425 2000 4425 2225
Connection ~ 6025 2000
Connection ~ 4425 2225
Wire Wire Line
	5025 2975 5325 2975
Wire Wire Line
	5125 2500 5125 2350
Wire Wire Line
	5125 2350 4625 2350
Wire Wire Line
	4625 2350 4625 2500
Wire Wire Line
	4625 2700 4425 2700
Wire Wire Line
	4425 2225 4425 2700
Wire Wire Line
	5125 2700 5325 2700
Wire Wire Line
	5325 2700 5325 2975
Connection ~ 5325 2975
Wire Wire Line
	5325 2975 6025 2975
Connection ~ 7050 2000
Connection ~ 7050 2975
Text Notes 3800 5800 0    60   ~ 0
Choose between a regular\ncrystal or a ceramic resonator.
$Comp
L Device:R_US R2
U 1 1 5EA5CC28
P 2375 1325
F 0 "R2" H 2443 1371 50  0000 L CNN
F 1 "1K" H 2443 1280 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 1315 50  0001 C CNN
F 3 "~" H 2375 1325 50  0001 C CNN
	1    2375 1325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R3
U 1 1 5EA713B0
P 2375 1725
F 0 "R3" H 2443 1771 50  0000 L CNN
F 1 "1K" H 2443 1680 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 1715 50  0001 C CNN
F 3 "~" H 2375 1725 50  0001 C CNN
	1    2375 1725
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R4
U 1 1 5EA7393A
P 2375 2125
F 0 "R4" H 2443 2171 50  0000 L CNN
F 1 "1K" H 2443 2080 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 2115 50  0001 C CNN
F 3 "~" H 2375 2125 50  0001 C CNN
	1    2375 2125
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R5
U 1 1 5EA73DFE
P 2375 2525
F 0 "R5" H 2443 2571 50  0000 L CNN
F 1 "1K" H 2443 2480 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 2515 50  0001 C CNN
F 3 "~" H 2375 2525 50  0001 C CNN
	1    2375 2525
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R6
U 1 1 5EA75C1F
P 2375 2925
F 0 "R6" H 2443 2971 50  0000 L CNN
F 1 "1K" H 2443 2880 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 2915 50  0001 C CNN
F 3 "~" H 2375 2925 50  0001 C CNN
	1    2375 2925
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R7
U 1 1 5EA76258
P 2375 3325
F 0 "R7" H 2443 3371 50  0000 L CNN
F 1 "1K" H 2443 3280 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 3315 50  0001 C CNN
F 3 "~" H 2375 3325 50  0001 C CNN
	1    2375 3325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R9
U 1 1 5EA79F18
P 2375 4175
F 0 "R9" H 2443 4221 50  0000 L CNN
F 1 "1K" H 2443 4130 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 4165 50  0001 C CNN
F 3 "~" H 2375 4175 50  0001 C CNN
	1    2375 4175
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R8
U 1 1 5EA7AA16
P 2375 3775
F 0 "R8" H 2443 3821 50  0000 L CNN
F 1 "1K" H 2443 3730 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 3765 50  0001 C CNN
F 3 "~" H 2375 3775 50  0001 C CNN
	1    2375 3775
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R10
U 1 1 5EA7D9A9
P 2375 4575
F 0 "R10" H 2443 4621 50  0000 L CNN
F 1 "1K" H 2443 4530 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 4565 50  0001 C CNN
F 3 "~" H 2375 4575 50  0001 C CNN
	1    2375 4575
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R11
U 1 1 5EA7E41D
P 2375 4975
F 0 "R11" H 2443 5021 50  0000 L CNN
F 1 "1K" H 2443 4930 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 4965 50  0001 C CNN
F 3 "~" H 2375 4975 50  0001 C CNN
	1    2375 4975
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R12
U 1 1 5EA7E96F
P 2375 5375
F 0 "R12" H 2443 5421 50  0000 L CNN
F 1 "1K" H 2443 5330 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 5365 50  0001 C CNN
F 3 "~" H 2375 5375 50  0001 C CNN
	1    2375 5375
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R13
U 1 1 5EA7F0E1
P 2375 5775
F 0 "R13" H 2443 5821 50  0000 L CNN
F 1 "1K" H 2443 5730 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 5765 50  0001 C CNN
F 3 "~" H 2375 5775 50  0001 C CNN
	1    2375 5775
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R14
U 1 1 5EA80A37
P 2375 6175
F 0 "R14" H 2443 6221 50  0000 L CNN
F 1 "1K" H 2443 6130 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 6165 50  0001 C CNN
F 3 "~" H 2375 6175 50  0001 C CNN
	1    2375 6175
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R15
U 1 1 5EA80DE4
P 2375 6575
F 0 "R15" H 2443 6621 50  0000 L CNN
F 1 "1K" H 2443 6530 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 6565 50  0001 C CNN
F 3 "~" H 2375 6575 50  0001 C CNN
	1    2375 6575
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 5EA8D48C
P 2375 925
F 0 "R1" H 2443 971 50  0000 L CNN
F 1 "1K" H 2443 880 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 915 50  0001 C CNN
F 3 "~" H 2375 925 50  0001 C CNN
	1    2375 925 
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5EA8EB4B
P 2900 2725
F 0 "SW5" H 2900 2918 50  0000 C CNN
F 1 "SW_Push" H 2900 2919 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2900 2925 50  0001 C CNN
F 3 "~" H 2900 2925 50  0001 C CNN
	1    2900 2725
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5EAA1EB9
P 2900 2325
F 0 "SW4" H 2900 2518 50  0000 C CNN
F 1 "SW_Push" H 2900 2519 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2900 2525 50  0001 C CNN
F 3 "~" H 2900 2525 50  0001 C CNN
	1    2900 2325
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1125 2375 1125
Connection ~ 2375 1125
Wire Wire Line
	2375 1125 2375 1175
Wire Wire Line
	3100 1125 3425 1125
Wire Wire Line
	3425 1125 3425 1525
Connection ~ 3425 1525
$Comp
L Switch:SW_Push SW6
U 1 1 5EAD1D32
P 2900 3125
F 0 "SW6" H 2900 3318 50  0000 C CNN
F 1 "SW_Push" H 2900 3319 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2900 3325 50  0001 C CNN
F 3 "~" H 2900 3325 50  0001 C CNN
	1    2900 3125
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5EAF6F12
P 2900 1925
F 0 "SW3" H 2900 2118 50  0000 C CNN
F 1 "SW_Push" H 2900 2119 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2900 2125 50  0001 C CNN
F 3 "~" H 2900 2125 50  0001 C CNN
	1    2900 1925
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5EAF7514
P 2900 1525
F 0 "SW2" H 2900 1718 50  0000 C CNN
F 1 "SW_Push" H 2900 1719 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2900 1725 50  0001 C CNN
F 3 "~" H 2900 1725 50  0001 C CNN
	1    2900 1525
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5EAF7BD7
P 2900 1125
F 0 "SW1" H 2900 1318 50  0000 C CNN
F 1 "SW_Push" H 2900 1319 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2900 1325 50  0001 C CNN
F 3 "~" H 2900 1325 50  0001 C CNN
	1    2900 1125
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5EB0551B
P 2925 3550
F 0 "SW7" H 2925 3743 50  0000 C CNN
F 1 "SW_Push" H 2925 3744 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 3750 50  0001 C CNN
F 3 "~" H 2925 3750 50  0001 C CNN
	1    2925 3550
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5EB05AA7
P 2925 3975
F 0 "SW8" H 2925 4168 50  0000 C CNN
F 1 "SW_Push" H 2925 4169 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 4175 50  0001 C CNN
F 3 "~" H 2925 4175 50  0001 C CNN
	1    2925 3975
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW9
U 1 1 5EB0AC68
P 2925 4375
F 0 "SW9" H 2925 4568 50  0000 C CNN
F 1 "SW_Push" H 2925 4569 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 4575 50  0001 C CNN
F 3 "~" H 2925 4575 50  0001 C CNN
	1    2925 4375
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW10
U 1 1 5EB10E4E
P 2925 4775
F 0 "SW10" H 2925 4968 50  0000 C CNN
F 1 "SW_Push" H 2925 4969 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 4975 50  0001 C CNN
F 3 "~" H 2925 4975 50  0001 C CNN
	1    2925 4775
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW11
U 1 1 5EB112D9
P 2925 5175
F 0 "SW11" H 2925 5368 50  0000 C CNN
F 1 "SW_Push" H 2925 5369 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 5375 50  0001 C CNN
F 3 "~" H 2925 5375 50  0001 C CNN
	1    2925 5175
	1    0    0    -1  
$EndComp
Wire Wire Line
	2375 5175 2725 5175
$Comp
L Switch:SW_Push SW12
U 1 1 5EB13992
P 2925 5575
F 0 "SW12" H 2925 5768 50  0000 C CNN
F 1 "SW_Push" H 2925 5769 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 5775 50  0001 C CNN
F 3 "~" H 2925 5775 50  0001 C CNN
	1    2925 5575
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW13
U 1 1 5EB156AE
P 2925 5975
F 0 "SW13" H 2925 6168 50  0000 C CNN
F 1 "SW_Push" H 2925 6169 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 6175 50  0001 C CNN
F 3 "~" H 2925 6175 50  0001 C CNN
	1    2925 5975
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW14
U 1 1 5EB167D5
P 2925 6375
F 0 "SW14" H 2925 6568 50  0000 C CNN
F 1 "SW_Push" H 2925 6569 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 6575 50  0001 C CNN
F 3 "~" H 2925 6575 50  0001 C CNN
	1    2925 6375
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3975 4200 4350
Wire Wire Line
	2375 6875 2725 6875
Connection ~ 2375 6875
Wire Wire Line
	2375 6875 2375 6925
$Comp
L Device:R_US R16
U 1 1 5EB9E894
P 2375 7075
F 0 "R16" H 2443 7121 50  0000 L CNN
F 1 "1K" H 2443 7030 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 7065 50  0001 C CNN
F 3 "~" H 2375 7075 50  0001 C CNN
	1    2375 7075
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW15
U 1 1 5EB9E89A
P 2925 6875
F 0 "SW15" H 2925 7068 50  0000 C CNN
F 1 "SW_Push" H 2925 7069 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 7075 50  0001 C CNN
F 3 "~" H 2925 7075 50  0001 C CNN
	1    2925 6875
	1    0    0    -1  
$EndComp
Wire Wire Line
	2375 6725 2375 6875
Wire Wire Line
	2375 7225 2375 7275
Wire Wire Line
	2375 7275 2725 7275
Connection ~ 2375 7275
Wire Wire Line
	2375 7275 2375 7325
$Comp
L Device:R_US R17
U 1 1 5EBBCC0A
P 2375 7475
F 0 "R17" H 2443 7521 50  0000 L CNN
F 1 "1K" H 2443 7430 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2415 7465 50  0001 C CNN
F 3 "~" H 2375 7475 50  0001 C CNN
	1    2375 7475
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW16
U 1 1 5EBBCC10
P 2925 7275
F 0 "SW16" H 2925 7468 50  0000 C CNN
F 1 "SW_Push" H 2925 7469 50  0001 C CNN
F 2 "Local_Footprints:PCB_PUSH_6mm_alignment_hole" H 2925 7475 50  0001 C CNN
F 3 "~" H 2925 7475 50  0001 C CNN
	1    2925 7275
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 6375 3425 6875
Wire Wire Line
	3425 7275 3125 7275
Connection ~ 3425 6375
Wire Wire Line
	3125 6875 3425 6875
Connection ~ 3425 6875
Wire Wire Line
	3425 6875 3425 7275
$Comp
L Device:R_US R22
U 1 1 5EC7F1EC
P 5500 3725
F 0 "R22" H 5568 3771 50  0000 L CNN
F 1 "330" H 5568 3680 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5540 3715 50  0001 C CNN
F 3 "~" H 5500 3725 50  0001 C CNN
	1    5500 3725
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R21
U 1 1 5EC862FE
P 5800 3725
F 0 "R21" H 5868 3771 50  0000 L CNN
F 1 "330" H 5868 3680 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5840 3715 50  0001 C CNN
F 3 "~" H 5800 3725 50  0001 C CNN
	1    5800 3725
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R18
U 1 1 5ECC4F35
P 4350 3975
F 0 "R18" V 4145 3975 50  0000 C CNN
F 1 "100K" V 4236 3975 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4390 3965 50  0001 C CNN
F 3 "~" H 4350 3975 50  0001 C CNN
	1    4350 3975
	0    1    1    0   
$EndComp
Connection ~ 4200 3975
$Comp
L Device:R_US R19
U 1 1 5ECCD31B
P 7150 3625
F 0 "R19" V 6945 3625 50  0000 C CNN
F 1 "150" V 7036 3625 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7190 3615 50  0001 C CNN
F 3 "~" H 7150 3625 50  0001 C CNN
	1    7150 3625
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R20
U 1 1 5ECCE4D2
P 7575 3625
F 0 "R20" V 7370 3625 50  0000 C CNN
F 1 "150" V 7461 3625 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7615 3615 50  0001 C CNN
F 3 "~" H 7575 3625 50  0001 C CNN
	1    7575 3625
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5ECDC7AB
P 8925 4500
F 0 "C2" H 9040 4546 50  0000 L CNN
F 1 "0.1uF" H 9040 4455 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 8963 4350 50  0001 C CNN
F 3 "~" H 8925 4500 50  0001 C CNN
	1    8925 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 4125 8925 4250
Wire Wire Line
	8875 4250 8925 4250
Connection ~ 8925 4250
Wire Wire Line
	8925 4250 8925 4350
Wire Wire Line
	8925 4650 8925 4750
Connection ~ 8925 4750
$Comp
L Device:C C5
U 1 1 5ED0AB87
P 7375 3775
F 0 "C5" H 7490 3821 50  0000 L CNN
F 1 "0.33uF" H 7490 3730 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 7413 3625 50  0001 C CNN
F 3 "~" H 7375 3775 50  0001 C CNN
	1    7375 3775
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3625 7375 3625
Wire Wire Line
	7725 3625 7875 3625
Connection ~ 7375 3625
Wire Wire Line
	7375 3625 7425 3625
$Comp
L Device:C C6
U 1 1 5ED14A43
P 7875 3775
F 0 "C6" H 7990 3821 50  0000 L CNN
F 1 "0.33uF" H 7990 3730 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 7913 3625 50  0001 C CNN
F 3 "~" H 7875 3775 50  0001 C CNN
	1    7875 3775
	1    0    0    -1  
$EndComp
Connection ~ 7875 3625
Wire Wire Line
	7875 3625 7950 3625
Wire Wire Line
	7375 3925 7875 3925
Wire Wire Line
	7875 3925 8275 3925
Connection ~ 7875 3925
Connection ~ 8275 3925
Wire Wire Line
	7050 2775 7300 2775
Wire Wire Line
	7050 2225 7300 2225
$Comp
L Device:CP1 C1
U 1 1 5ED313DA
P 7050 2500
F 0 "C1" H 7165 2546 50  0000 L CNN
F 1 "22uF" H 7165 2455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D5.0mm_P2.50mm" H 7050 2500 50  0001 C CNN
F 3 "~" H 7050 2500 50  0001 C CNN
	1    7050 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 2650 7050 2775
Connection ~ 7050 2775
Wire Wire Line
	7050 2350 7050 2225
Connection ~ 7050 2225
$Comp
L Device:D D1
U 1 1 5ED40DDF
P 6650 2000
F 0 "D1" H 6650 1784 50  0000 C CNN
F 1 "1N4005" H 6650 1875 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P7.62mm_Horizontal" H 6650 2000 50  0001 C CNN
F 3 "~" H 6650 2000 50  0001 C CNN
	1    6650 2000
	-1   0    0    1   
$EndComp
$Comp
L Device:C C7
U 1 1 5ED644FB
P 8100 3625
F 0 "C7" V 7848 3625 50  0000 C CNN
F 1 "0.33uF" V 7939 3625 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 8138 3475 50  0001 C CNN
F 3 "~" H 8100 3625 50  0001 C CNN
	1    8100 3625
	0    1    1    0   
$EndComp
Wire Wire Line
	8250 3625 8525 3625
$Comp
L Device:C C4
U 1 1 5EAA7A73
P 3975 4750
F 0 "C4" V 4100 4700 50  0000 L CNN
F 1 "22pF" V 4050 4825 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4013 4600 50  0001 C CNN
F 3 "~" H 3975 4750 50  0001 C CNN
	1    3975 4750
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5EACD16F
P 3975 4450
F 0 "C3" V 3900 4525 50  0000 L CNN
F 1 "22pF" V 3825 4375 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4013 4300 50  0001 C CNN
F 3 "~" H 3975 4450 50  0001 C CNN
	1    3975 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	3575 4600 3825 4600
Wire Wire Line
	3825 4450 3825 4600
Connection ~ 3825 4600
Wire Wire Line
	3825 4600 4250 4600
Wire Wire Line
	3825 4600 3825 4750
$Comp
L Device:Crystal_GND2 X1
U 1 1 5EB2F410
P 4450 4600
F 0 "X1" V 4404 4731 50  0000 L CNN
F 1 "20 MHz" V 4495 4731 50  0000 L CNN
F 2 "Crystal:Crystal_HC49-U-3Pin_Vertical" H 4450 4600 50  0001 C CNN
F 3 "~" H 4450 4600 50  0001 C CNN
	1    4450 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	4125 4450 4450 4450
Wire Wire Line
	4125 4750 4450 4750
Connection ~ 4450 4450
Wire Wire Line
	4450 4450 5050 4450
Connection ~ 4450 4750
Wire Wire Line
	4450 4750 5050 4750
$EndSCHEMATC
